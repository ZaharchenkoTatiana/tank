namespace fireball {
public interface ICommand
{
    void Execute();
}

public interface UObject{
    object this[string key]
    {
        get;
        set;
    }
}

public interface Firebale{

    Vector InitalBulletPosition
    {
        get;
    }

    Vector InitalBulletDirection
    {
        get;
    }

    Vector InitialVelocity{
        get;
    }

}

}