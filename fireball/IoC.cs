using System;
using System.Runtime.CompilerServices;

namespace fireball {
public class IoC
{
     public static T resolve<T> (string key, params object[] args)
     {
         return (T)strategy(key, args);
     }

     public class IoCSetupCommand : ICommand
     {
         Func<string, object[], object> newStrategy;

         public IoCSetupCommand(Func<string, object[], object> newStrategy)
         {
             this.newStrategy = newStrategy;
         }

         public void Execute()
         {
             strategy = newStrategy;
         }
     }

    public static Func <string, object[], object> strategy = (key, args) =>
     { if ("IoC.Setup" == key)
        {
            var newStrategy = (Func<string,object[],object>) args[0];

            return new IoCSetupCommand(newStrategy);
        }
        else if ("IoC.Strategy" == key)
        {
            return strategy;
        }
        else
        {
            throw new Exception();
        }

     };
     
}
}