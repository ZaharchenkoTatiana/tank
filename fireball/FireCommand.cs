using System.Windows.Input;
using System;
namespace fireball {

public class FireCommand : ICommand {
        Firebale fireball;
 
        public FireCommand(Firebale fireball){
            this.fireball = fireball;
        }

        public void Execute()
        
        {
            var obj = IoC.resolve<UObject>("GameObjects.CreateBullet");
            IoC.resolve<ICommand>("GameObject.SetPosition", obj, fireball.InitalBulletPosition).Execute();
            IoC.resolve<ICommand>("GameObject.SetDirection", obj, fireball.InitalBulletDirection).Execute();
            var action = IoC.resolve<UObject>("Action", obj, "Move", fireball.InitialVelocity);
            IoC.resolve<ICommand>("GameObject.StartMovement", action).Execute();
        }

    }
}
