using System.Numerics;
using System;
using System.Collections.Generic;

namespace fireball
{
    public class Vector
    {
        int[] val;
        
        int x, y;

        public Vector(int[] val){

        this.val = val;
    }
    
    public Vector(int x, int y)
    {
        this.x = x;
        this.y = y;
    }    
       

    public override bool Equals(Object obj)
    {
        Vector p = obj as Vector; 
            
        return  (x == p.x) && (y == p.y);
            
    }

    public override int GetHashCode()    
    {
        return (x << 2) ^ y;
    }

    public static Vector operator +(Vector v1, Vector v2)
        {
            return new Vector(v1.x + v2.x, v1.y + v2.y);
        }
    
    } 
} 
    
        
       
        
        
          

      
        
